<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('products', [\App\Http\Controllers\ProductController::class, 'products']);
Route::post('detail_product', [\App\Http\Controllers\ProductController::class, 'show']);
Route::post('create_product', [\App\Http\Controllers\ProductController::class, 'create']);
Route::post('update_product', [\App\Http\Controllers\ProductController::class, 'update']);
Route::post('delete_product', [\App\Http\Controllers\ProductController::class, 'delete']);
