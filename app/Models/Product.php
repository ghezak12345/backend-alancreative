<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use File;

class Product extends Model
{
    use HasFactory;

    protected $table = 'master_produk';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'id','nama','foto','harga','deleted','created_by','updated_by','created_at','updated_at',
    ];

    public function addProduct($data){
        $product                = New Self;
        $product->nama          = $data->input('nama');

        // upload gambar
        $ekstensiongambar = $data->file('foto')->getClientOriginalExtension();
        $namagambar = $this->generateRandomString().'_'.date('Ymdhis').'.'.$ekstensiongambar;
        $data->file('foto')->move(storage_path('app/public'), $namagambar);
        
        $product->foto          = $namagambar;
        $product->harga         = $data->input('harga');

        $product->created_by    = 999;
        $product->updated_by    = 999;

        if($product->save()){
            return $product->id;
        }else{
            return false;
        }
    }

    public function updateProduct($data){
        $product                = Self::where('id', $data->input('id'))->first();
        
        if(!$product){
            return false;
        }
        
        $product->nama          = $data->input('nama');
        
        if ($data->hasFile('foto')) {
            $ekstensiongambar = $data->file('foto')->getClientOriginalExtension();
            $namagambar = $this->generateRandomString().'_'.date('Ymdhis').'.'.$ekstensiongambar;
            $data->file('foto')->move(storage_path('app/public'), $namagambar);

            if(File::exists(public_path('/storage/'.$product->foto))){
                File::delete(public_path('/storage/'.$product->foto));
            }

            $product->foto =  $namagambar;
        } 
        
        $product->harga         = $data->input('harga');

        $product->updated_by    = 999;
        $product->updated_at    = Carbon::now()->timezone('Asia/Jakarta');

        if($product->save()){
            return $product->id;
        }else{
            return false;
        }
    }

    public function deleteProduct($data){
        $product = Self::where('id', $data->input('id'))->first();

        if(!$product){
            return false;
        }
        
        $product->deleted       = 'Y';
        $product->updated_by    = 999;
        $product->updated_at    = Carbon::now()->timezone('Asia/Jakarta');
        
        if($product->save()){
            return $product->id;
        }else{
            return false;
        }
    } 

    protected function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
