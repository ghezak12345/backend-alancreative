<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    
    public function products(){
        try {
            $getData = Product::where('deleted', 'N')->get();
            return response()->json([ 'status' => true, 'data' => $getData, 'message' => ($getData->isEmpty() ? 'Data Empty!!!' : '') ]);
        } catch (\Throwable $th) {
            return response()->json([ 'status' => false, 'data' => null, 'message' => 'Server Error Please try again later!!!' ])->setStatusCode(500);
        }
    }

    public function show(Request $request){
        try {
            $this->validateAndRespond($request, [
                'id' => 'required|integer',
            ]);
        } catch (HttpResponseException $e) {
            return $e->getResponse();
        }

        $id = $request->input('id');
        
        try {
            $model = Product::findOrFail($id);
            return response()->json([ 'status' => true, 'data' => $model, 'message' => '' ]);
        } catch (\Throwable $th) {
            return response()->json([ 'status' => false, 'data' => null, 'message' => 'Data Not Found!!' ])->setStatusCode(404);
        }
    }

    public function create(Request $request){
        try {
            $this->validateAndRespond($request, [
                'nama'  => 'required|string',
                'foto'  => 'required|file',
                'harga' => 'required|string',
            ]);
        } catch (HttpResponseException $e) {
            return $e->getResponse();
        }

        $product      = new Product();
        try {
            $product->addProduct($request);
            return response()->json([ 'status' => true, 'message' => 'Product Berhasil di buat']);
        } catch (\Throwable $th) {
            return response()->json([ 'status' => false, 'message' => 'Server Error Please try again later!!!' ])->setStatusCode(500);
        }
    }

    public function update(Request $request){
        try {
            $this->validateAndRespond($request, [
                'id'  => 'required|string',
                'nama'  => 'required|string',
                'harga' => 'required|string',
            ]);
        } catch (HttpResponseException $e) {
            return $e->getResponse();
        }

        $product = new Product();
        try {
            $updateProduct = $product->updateProduct($request);
            if($updateProduct){
                return response()->json([ 'status' => true, 'message' => 'Product Berhasil di ubah']);
            }else{
                return response()->json([ 'status' => true, 'message' => 'Product Gagal di ubah']);
            }
        } catch (\Throwable $th) {
            return response()->json([ 'status' => false, 'message' => $th->getMessage() ])->setStatusCode(500);
        }
    }

    public function delete(Request $request){
        try {
            $this->validateAndRespond($request, [
                'id' => 'required|integer',
            ]);
        } catch (HttpResponseException $e) {
            return $e->getResponse();
        }

        $product = new Product();
        try {
            $deleteProduct = $product->deleteProduct($request);
            if($deleteProduct){
                return response()->json([ 'status' => true, 'message' => 'Product Berhasil di hapus']);
            }else{
                return response()->json([ 'status' => true, 'message' => 'Product Gagal di hapus']);
            }
        } catch (\Throwable $th) {
            return response()->json([ 'status' => false, 'message' => 'Server Error Please try again later!!!' ])->setStatusCode(500);
        }
    }

    protected function validateAndRespond($request, $rules){
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            throw new HttpResponseException(response()->json([
                'status' => false,
                'message' => 'Validation failed',
                'errors' => $validator->errors()
            ], 422));
        }
    }

}
